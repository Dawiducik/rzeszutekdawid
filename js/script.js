
"use strict"
var flexContainerHeight = function() {
    var header = document.getElementById('main-header'),
        nav = document.getElementById('main-nav'),
        container = document.getElementById('flexContainer'),
        //footer = document.getElementById('footer'),
        windowHeight = window.innerHeight,
        offsetsHeight = header.offsetHeight + nav.offsetHeight + 0,
        flexContainerHeight = windowHeight - offsetsHeight;
    container.setAttribute("style","min-height: " + flexContainerHeight + "px");
}
document.addEventListener("DOMContentLoaded", function() {
    flexContainerHeight();
    
});
window.addEventListener('resize', function(event){
    flexContainerHeight();
});

